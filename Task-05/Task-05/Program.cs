﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_05
{
    class Program
    {
        static void Main(string[] args)
        {
            greeting("Jeremy");
        }
        static void greeting(string name)
        {
            Console.WriteLine($"Hello {name}");
        }
    }
}
